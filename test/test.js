

const StudentManagementSystem = artifacts.require("StudentManagementSystem");

contract("StudentManagementSystem", async accounts=> {
  it("student details added", async() => {
    sms = await StudentManagementSystem.deployed();
    roll = 12;
    name ="Anu";
    age = 18;
    indian = 1;
    gender=1;
    try{
      await sms.setStudent(roll,name,age,indian,gender);
      studentRegisteredDetails= await sms.getStudent(roll);
      console.log("studentRegisteredDetails",studentRegisteredDetails);
      assert.equal(studentRegisteredDetails[0],name,"Test fail");
      assert.equal(studentRegisteredDetails[1],age,"Test fail");
      assert.equal(studentRegisteredDetails[2],indian,"Test fail");
      assert.equal(studentRegisteredDetails[3],gender,"Test fail");


    }
    catch(err){
      assert(err);
      console.log(err);

    }
  });  
  it("fee payment",async()=>{
    sms = await StudentManagementSystem.deployed();
    // console.log("deployed sms",sms);

    studentAddress= accounts[1];
    smsAddress= await sms.address;
    contractBalance= await web3.eth.getBalance(smsAddress);
    console.log("===============Contract Balance",contractBalance);
    roll= 1;
    amountver =3;
    try{
      await sms.feePayment(roll,{from:studentAddress, value:web3.utils.toWei(amountver.toString(),'ether')});
      CurrentContractBalance= await web3.eth.getBalance(smsAddress);
      console.log("====>CurrentContractBalance",CurrentContractBalance);
      Difference= CurrentContractBalance-contractBalance;
      DifferenceInEther=await web3.utils.fromWei(Difference.toString(),"ether");
      console.log("====>Difference",DifferenceInEther);
      assert.equal(DifferenceInEther,amountver,"Test fail");
      
      

    }
    catch(err){
      assert(err); 
      console.log(err);
    }


 

  })
  //checking withdraw  functionality for management
it('Withdraw Fees from contract Account', async () => {
  sms = await StudentManagementSystem.deployed();
  Management =accounts[0];
  console.log("amountver",amountver);

  try {
     
      contractBalance = await web3.eth.getBalance(smsAddress);
      contractBalanceEther = await web3.utils.fromWei(contractBalance, 'ether');
      console.log(contractBalanceEther);
      x = await web3.eth.getBalance(Management);
      console.log("=======>Management Balance",x);
      ManagementBalance = await Math.trunc(web3.utils.fromWei(x, 'ether'));
      console.log("ManagementBalance", ManagementBalance);

      smsAddress = await sms.address;
      console.log(smsAddress);

      await sms.feeWithdraw({ from: accounts[0]});
      contractBalance1 = await web3.eth.getBalance(smsAddress);
      console.log("contractBalance1", contractBalance1);
      CurrentManagementBalance1 = await web3.eth.getBalance(Management);
      console.log(CurrentManagementBalance1);
      CurrentManagementBalance = Math.trunc(web3.utils.fromWei(CurrentManagementBalance1, 'ether'))
      console.log("================>CurrentManagementBalance", CurrentManagementBalance);
      Difference = CurrentManagementBalance - ManagementBalance;
      console.log("============>Difference", Difference);
      assert.equal(Difference, contractBalanceEther, "Test Fail");
  }
  catch (err) {
      assert(err);
      console.log(err);
  }
});
 
});



